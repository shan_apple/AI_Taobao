<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <title>美拍</title>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" />
    <link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
<header id="header">
    <a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">找回密码</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
</header>
<div class="content">
    <div class="register-box">
        <div class="form-box">
            <form name="form1" action="useradd.do" method="post">
                <div class="row row-input clearfix" id="div1">
                    <div class="row-l fl"><i class="iconfont">&#xe64f;</i></div>
                    <div class="row-r">
                        <input type="text" class="input_box"  placeholder="输入手机号" id="phone">
                    </div>
                </div>
                <div class="row row-input clearfix"  id="div2">
                    <div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
                    <div class="row-r">
                        <input type="text" class="input_box" placeholder="输入验证码" id="verification">
                    </div>
                </div>
                <div class="row row-input clearfix" id="div3">
                    <div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
                    <div class="row-r">
                        <input type="password" class="input_box" placeholder="输入密码" id="password">
                    </div>
                </div>
                <div class="row row-input clearfix" id="div4">
                    <div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
                    <div class="row-r">
                        <input type="password" class="input_box" placeholder="再次输入密码" id="password1">
                    </div>
                </div>

                <div class="row row-btn" id="div5">
                    <a href="javascript:void(0);"class="green-btn" id="tip">确定</a>
                </div>


            </form>
        </div>
    </div>


<%--不知道为啥这一行不能删，否则下面/body会提示错误 --%>
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>

<script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>

<script >
    $(document).ready(function(){
        $("#div3").hide();
        $("#div4").hide();
        $("#div5").hide();

    });


    layui.use('layer', function(){ //独立版的layer无需执行这一句
        var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        //触发事件
        var phoneFlag=0;
        function success (str) {
            layer.msg(str, {
                icon:1,
                offset: 't',//设置距离 上,左 位置['40%','30%']
                anim: 5//出现方式为渐显
            });
        }
        function error (str) {
            layer.msg(str, {
                offset: 't',//设置距离 上,左 位置['40%','30%']
                anim: 6//出现方式为抖动
            });
        }

        $("#phone").bind('input propertychange',function(){
            phoneFlag=1;
            var phone=document.getElementById("phone").value;
            if(phone.length>11||isNaN(phone)){
                error("手机号填写错误");
                document.getElementById("phone").value=phone.substring(0,11);
                return ;
            }
        });
        $("#verification").bind('input propertychange',function(){
            var phoneIsExist=1;
            var phone=document.getElementById("phone").value;
            var code=document.getElementById("verification").value;
            if(phoneFlag==1){
                if(phone.length!=11||isNaN(phone)){
                    error("手机号填写错误");
                    document.getElementById("verification").value="";
                    return ;
                }
                $.post("All?method=userForgetPasswordDo",
                    {
                        phone:phone
                    },
                    function(data,status){
                        var number=data.flag;
                        if(number=="0"){
                            error("该用户不存在");
                            document.getElementById("verification").value="";

                            phoneIsExist=0;
                        }
                        else{
                            phoneFlag=0;
                        }
                    }
                    ,"json"
                );
            }
            if(phoneIsExist==0){
                return;
            }
            if(code.length>=6){
                if(code!="123456"){
                    document.getElementById("verification").value="";
                    error("验证码错误!!!");
                    return;
                }
                else{
                    success("验证成功");
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").show();
                    $("#div4").show();
                    $("#div5").show();
                }
            }

        })


        $("#tip").on('click', function (message){
            var phone=document.getElementById("phone").value;
            var password=document.getElementById("password").value;
            var password1=document.getElementById("password1").value;
            if(password==null||password.length<6){
                error("密码长度不得小于六位");
                return;
            };
            if(password!=password1){
                error("两次密码不匹配");
                return;
            }

            $.post("All?method=userForgetPasswordDo",
                {
                    phone:phone,
                    password:password
                },
                function(data,status){
                    var number=data.flag;
                    if(number=="0"){
                        error("修改失败");
                    }
                    else if(number=="1"){
                        success("修改成功");
                    }
                }
                ,"json"
            );

        });
    });
</script>


</body>
</html>
