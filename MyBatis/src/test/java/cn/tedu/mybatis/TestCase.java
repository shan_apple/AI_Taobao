package cn.tedu.mybatis;

import cn.edu.config.MybatisConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestCase {
    AnnotationConfigApplicationContext ctx;
    @Before
    public void init(){
        ctx = new AnnotationConfigApplicationContext(MybatisConfig.class);
    }
    @After
    public void destroy(){
        ctx.close();
    }
    @Test
    public void testDataSource(){
        DataSource ds = ctx.getBean("dataSource",DataSource.class);
        String sql = "SELECT * FROM users where  phone='17838469536'";
        try (Connection conn = ds.getConnection()){
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()){
                System.out.println(rs.getString("phone"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
