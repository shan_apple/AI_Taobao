<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="cn.edu.lit.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">我的收藏</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content mei_list">
	<%--<div class="top-nav1">
  		<ul class="clearfix">
  			<li><span class="now"><a href="javascript:void (0)" onclick="meiList()">美拍</a></span></li>
  			&lt;%&ndash;<li><span ><a href="javascript:void (0)" onclick="tieList()">美贴</a></span></li>
  			<li><span><a href="#">美言</a></span></li>
            <li><span><a href="#">美视</a></span></li>&ndash;%&gt;
  		</ul>
  	</div>--%>
	<div class="card-list" id="meiList">
  		<div class="con-list">
  	 		<ul class="clearfix">
				<c:forEach var="mei" items="${meiAll }" >
					<li >
						<div class="photo-card">
							<a href="All?method=meiDetailGo&meiId=${mei.id}" class="img"  onclick=""><img src="${mei.img}" ></a>
							<p class="txt"><a href="" >${mei.title}</a></p>
							<div class="info clearfix">
								<a href="javascript:void(0)" onclick="">
                    <span class="author">
                      <img src="${mei.photo}">
                      <span class="author">${mei.username }</span>
                    </span>
								</a>
								<a href="javascript:void(0)" onclick="meiPraise(${mei.id})">
                    <span class="zan">
                      <i class="iconfont" id="meiPraseIcon${mei.id}" style="color: ${mei.praiseColor}">&#xe600;</i>
                      <span class="zan" id="meiPraseCount${mei.id}">${mei.praise }</span>
                    </span></a>
								<a href="javascript:void(0)" onclick="meiCollect(${mei.id})">
                    <span class="collect">
                      <i class="iconfont" id="meiCollectIcon${mei.id}" style="color: ${mei.collectColor}">&#xe605;</i>
                      <span class="collect" id="meiCollectCount${mei.id}">${mei.collect }</span>
                    </span>
								</a>
							</div>
						</div>
					</li>
				</c:forEach>
  	 			
  	 		</ul>

  	 	</div>
  	</div>

	  <div class="column column-4 imgtxt-list" id="tieList">
		  <div class="con-list">
			  <ul class="clearfix">
				  <c:forEach var="tie" items="${tieAll }" >
					  <li >
						  <div class="li-box clearfix">
							  <a href="All?method=tieDetailGo&tieId=${tie.id}" class="img  fl"  onclick=""><img src="${tie.img}" ></a>
							  <div class="txt-box">
								  <p class="txt"><a href="All?method=tieDetailGo&tieId=${tie.id}" >${tie.title}</a></p>
								  <div class="info clearfix">
									  <a href="javascript:void(0)" onclick="tiePraise(${tie.id})">
                    <span class="zan">
                      <i class="iconfont" id="tiePraseIcon${tie.id}" style="color: ${tie.praiseColor}">&#xe600;</i>
                      <aaa id="tiePraseCount${tie.id}">${tie.praise }</aaa>
                    </span></a>
									  <a href="javascript:void(0)" onclick="tieCollect(${tie.id})">
                    <span class="collect">
                      <i class="iconfont" id="tieCollectIcon${tie.id}" style="color: ${tie.collectColor}">&#xe605;</i>
                      <aaa id="tieCollectCount${tie.id}">${tie.collect }</aaa>
                    </span>
									  </a>
									  <a href="javascript:void(0)" onclick="">
                    <span class="author">
                      <img src="${tie.photo}">${tie.username }
                    </span>
									  </a>
								  </div>
							  </div>
						  </div>
					  </li>
				  </c:forEach>
			  </ul>
		  </div>
	  </div>
     
  </div>
 
  <%@ include file="foot.jsp" %>

  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <script src="resources/layui/layui.js" charset="utf-8"></script>
  <script >
	  document.getElementById("foot1").classList.add("active");
	  $('#tieList').hide();



	  layui.use('layer', function(){ //独立版的layer无需执行这一句
		  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		  //触发事件
		  function success () {
			  layer.msg("注册成功", {
				  icon:1,
				  offset: 't',//设置距离 上,左 位置['40%','30%']
				  anim: 5//出现方式为渐显
			  });
		  }
		  function error (str) {
			  layer.msg(str, {
				  offset: 't',//设置距离 上,左 位置['40%','30%']
				  anim: 6//出现方式为抖动
			  });
		  }

		  window.meiDetailGo=function(meiId){
			  window.location.href="All?method=userLoginGo";
		  }

		  window.meiPraise=function(meiId){
			  var praise=document.getElementById("meiPraseCount"+meiId);
			  var icon=document.getElementById("meiPraseIcon"+meiId);
				  //646464  FF0000红
			  $.post("All?method=meiPraise",
					  {
						  id:meiId
					  },
					  function(data,status){
						  var flagColor1=data.flagColor;
						  if(flagColor1=="0"){
							  icon.style.color="#646464";
							  praise.innerText=parseInt(praise.innerText)-1;
						  }
						  else if(flagColor1=="1"){
							  icon.style.color="#FF0000";
							  praise.innerText=parseInt(praise.innerText)+1;
						  }
					  }
					  ,"json"
			  );




		  }
		  window.meiCollect=function(meiId){
			  layer.confirm('是否取消收藏？', {
				  btn: ['是','否'] //按钮
			  }, function(){
				  var collect=document.getElementById("meiCollectCount"+meiId);
				  var icon=document.getElementById("meiCollectIcon"+meiId);
				  $.post("All?method=meiCollect",
						  {
							  id:meiId
						  },
						  function(data,status){

						  }
						  ,"json"
				  );
				  $('#li'+meiId).hide();
				  layer.closeAll('dialog');
			  }, function(){
			  });
		  }

		  window.tieList=function(){
			  $('#meiList').hide();
			  $('#tieList').show();
		  }

		  window.meiList=function(){
			  $('#tieList').hide();
			  $('#meiList').show();
		  }


	  });
  </script>

</body>
</html>
