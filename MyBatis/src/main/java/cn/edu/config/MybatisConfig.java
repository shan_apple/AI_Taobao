package cn.edu.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;


import javax.sql.DataSource;
//用来扫描的
@Configuration
@PropertySource("classpath:jdbc.properties")
public class MybatisConfig {
    @Value("${db.driver}") String driver;
    @Value("${db.url}") String url;
    @Value("${db.username}") String username;
    @Value("${db.password}") String password;
    @Value("${db.maxActive}") int maxActive;
    @Value("${db.initialSize}") int initialSize;

    @Bean
    public DataSource dataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaxActive(maxActive);
        dataSource.setInitialSize(initialSize);
        return  dataSource;
    }
    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource,
                @Value("classpath:mappers/*.xml")
                        Resource[] mapperLocations
            ) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(mapperLocations);
        return bean.getObject();
    }

}
