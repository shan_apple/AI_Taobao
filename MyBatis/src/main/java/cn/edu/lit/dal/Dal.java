package cn.edu.lit.dal;

import cn.edu.config.MybatisConfig;
import cn.edu.lit.common.Constants;
import cn.edu.lit.model.Mei;
import cn.edu.lit.model.Tie;
import cn.edu.lit.model.User;
import cn.edu.lit.util.DbUtilC3P0;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Dal {
    DbUtilC3P0 util = new DbUtilC3P0();


    //region about mei
    private User creatUser(ResultSet rs) {
        User user = new User();
        try {
            user.setPhone(rs.getString("phone"));
            user.setPassword(rs.getString("password"));
            user.setName(rs.getString("name"));
            user.setSex(rs.getString("sex"));
            user.setPhoto(rs.getString("photo"));
            user.setDescription(rs.getString("description"));
            user.setBalance(rs.getInt("balance"));
            user.setCash(rs.getInt("cash"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    private Mei creatMei(ResultSet rs, String phone) {
        Mei mei = new Mei();
        try {
            mei.setId(rs.getInt("id"));
            mei.setPhone(rs.getString("phone"));
            mei.setTitle(rs.getString("title"));
            mei.setPraise(rs.getInt("praise"));
            mei.setCollect(rs.getInt("collect"));
            mei.setImg(rs.getString("img"));
            mei.setUsername(rs.getString("name"));
            mei.setPhoto(rs.getString("photo"));
            if (phone == null || phone.length() == 0) {
                mei.setPraiseFlag(false);
                mei.setCollectFlag(false);
            } else {
                mei.setPraiseFlag(getMeiPraiseFlagByIdAndPhone(String.valueOf(mei.getId()), phone));
                mei.setCollectFlag(getMeiCollectFlagByIdAndPhone(String.valueOf(mei.getId()), phone));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return mei;
    }

    

    
    public User getUserByPhone(String phone) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        DbUtilC3P0 util = new DbUtilC3P0();
        String sql = null;
        int number = 0;
        User user = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from users where phone='%s'", phone);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                user = creatUser(rs);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return user;
    }


    public int userAdd(String phone, String password, String name, String sex) {
        Connection conn = null;
        Statement stat = null;
        DbUtilC3P0 util = new DbUtilC3P0();
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into users (phone,password,name,sex) " +
                    "values('%s','%s','%s','%s')", phone, password, name, sex);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int changePassword(String phone, String password) {
        Connection conn = null;
        Statement stat = null;
        DbUtilC3P0 util = new DbUtilC3P0();
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("update users set password='%s' where phone='%s'", password, phone);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        }
        return number;
    }

    public List<Mei> getMeis(User user) {
        AnnotationConfigApplicationContext ctx= new AnnotationConfigApplicationContext(MybatisConfig.class);
        //DataSource ds = ctx.getBean("dataSource", DataSource.class);

        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Mei> meis = null;
        meis = new ArrayList<Mei>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from meis,users where meis.phone=users.phone");
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Mei mei = creatMei(rs, phone);
                meis.add(mei);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return meis;
    }

    public List<Mei> getMeisByContSigns(List<String> contsigns,User user) {
        AnnotationConfigApplicationContext ctx= new AnnotationConfigApplicationContext(MybatisConfig.class);
        //DataSource ds = ctx.getBean("dataSource", DataSource.class);

        Connection conn = null;
        Statement stat = null;

        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Mei> meis = null;
        meis = new ArrayList<Mei>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            for (String contsign: contsigns) {
                ResultSet rs = null;
                sql = String.format("select * from meis ,users where meis.phone=users.phone and cont_sign='%s' limit 0,1",contsign);
                rs = stat.executeQuery(sql);
                while (rs.next()) {
                    Mei mei = creatMei(rs, phone);
                    meis.add(mei);
                }
                rs.close();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {

                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return meis;
    }

    

    public boolean getMeiPraiseFlagByIdAndPhone(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from meipraises where phone='%s' and id=%s", phone, id);
            rs = stat.executeQuery(sql);
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public int meiPraisesAdd(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into meipraises values(%s,'%s')", id, phone);
            number = stat.executeUpdate(sql);
            sql = String.format("update meis  set praise=praise+1 where id=%s ", id);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int meiPraisesDel(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("delete from meipraises where id=%s and phone='%s'", id, phone);
            number = stat.executeUpdate(sql);
            sql = String.format("update meis  set praise=praise-1 where id=%s ", id);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public boolean getMeiCollectFlagByIdAndPhone(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from meiCollectes where phone='%s' and id=%s", phone, id);
            rs = stat.executeQuery(sql);
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public int meiCollectAdd(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into meiCollectes values(%s,'%s')", id, phone);
            number = stat.executeUpdate(sql);
            sql = String.format("update meis  set collect=collect+1 where id=%s ", id);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int meiCollectDel(String id, String phone) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("delete from meiCollectes where id=%s and phone='%s'", id, phone);
            number = stat.executeUpdate(sql);
            sql = String.format("update meis  set collect=collect-1 where id=%s ", id);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }


    public int MeiAdd(String title, String phone, String img,String contsign) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into meis (phone,title,img,cont_sign) values('%s','%s','%s','%s')", phone, title, img,contsign);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int MeiDel(String title, String phone, String img) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into meis (phone,title,img) values('%s','%s','%s')", phone, title, img);
            number = stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public Mei getMeiById(String meiId, User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        Mei mei = new Mei();
        String phone = null;
        if (user != null) phone = user.getPhone();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from meis,users where meis.id=%s and meis.phone=users.phone", meiId);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                mei = creatMei(rs, phone);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return mei;
    }

    public void userChangeName(String phone, String name) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("update users set name='%s' where phone='%s'", name, phone);
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }

    public void userChangeSex(String phone, String sex) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("update users set sex='%s' where phone='%s'", sex, phone);
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public void userChangeDescription(String phone, String description) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("update users set description='%s' where phone='%s'", description, phone);
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public List<Mei> getMeiCollectByPhone(User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Mei> meis = null;
        meis = new ArrayList<Mei>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            /*sql = String.format("select * from meis,users,meicollectes where meis.phone=users.phone and" +
                   " users.phone=meicollectes.phone and " +
                    "meis.id=meicollectes.id and meicollectes.phone='%s'", phone);*/
            sql = String.format("select * from meis,users where meis.phone=users.phone and meis.id=86", phone);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Mei mei = creatMei(rs, phone);
                meis.add(mei);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return meis;
    }

    public void userTixian(User user, int i) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("update users set balance=balance-%d,cash=cash+%d where phone='%s'", i, i, user.getPhone());
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public List<Mei> getMeiMineList(User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Mei> meis = null;
        meis = new ArrayList<Mei>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from meis,users where meis.phone=users.phone and" +
                    " users.phone='%s'", phone);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Mei mei = creatMei(rs, phone);
                meis.add(mei);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return meis;
    }

    public void meiMineDel(String meiId) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("delete from meis where id=%s", meiId);
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public String getContSignById(String meiId) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        String contsign="";
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select cont_sign from meis where id=%s", meiId);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                contsign= rs.getString("cont_sign");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
                rs.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return  contsign;
    }

    //endregion me    M-----mei


    //##########################################

    public int TieAdd(String title, String phone, String img) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        int number = 0;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("insert into ties (phone,title,img) values('%s','%s','%s')", phone, title, img);
            number = stat.executeUpdate(sql);
            conn.close();
            stat.close();
        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return number;
    }

    public List<Tie> getTieMineList(User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Tie> ties = null;
        ties = new ArrayList<Tie>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from ties,users where ties.phone=users.phone and" +
                    " users.phone='%s'", phone);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Tie tie = creatTie(rs, phone);
                ties.add(tie);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return ties;
    }

    public void tieMineDel(String tieId) {
        Connection conn = null;
        Statement stat = null;
        String sql = null;
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("delete from ties where id=%s", tieId);
            stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public boolean getTiePraiseFlagByIdAndPhone(String id,String phone) {
        Connection conn=null;
        Statement stat =null;
        ResultSet rs=null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("select * from tiepraises where phone='%s' and id=%s",phone,id);
            rs=stat.executeQuery(sql);
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                if(rs!=null)rs.close();
                if(conn!=null)conn.close();
                if(stat!=null)stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public int tiePraisesAdd(String id, String phone) {
        Connection conn=null;
        Statement stat =null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("insert into tiepraises values(%s,'%s')",id,phone);
            number=stat.executeUpdate(sql);
            sql=String.format("update ties  set praise=praise+1 where id=%s ",id);
            number=stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int tiePraisesDel(String id, String phone) {
        Connection conn=null;
        Statement stat =null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("delete from tiepraises where id=%s and phone='%s'",id,phone);
            number=stat.executeUpdate(sql);
            sql=String.format("update ties  set praise=praise-1 where id=%s ",id);
            number=stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }
    public boolean getTieCollectFlagByIdAndPhone(String id,String phone) {
        Connection conn=null;
        Statement stat =null;
        ResultSet rs=null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("select * from tieCollectes where phone='%s' and id=%s",phone,id);
            rs=stat.executeQuery(sql);
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                if(rs!=null)rs.close();
                if(conn!=null)conn.close();
                if(stat!=null)stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public int tieCollectAdd(String id, String phone) {
        Connection conn=null;
        Statement stat =null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("insert into tieCollectes values(%s,'%s')",id,phone);
            number=stat.executeUpdate(sql);
            sql=String.format("update ties  set collect=collect+1 where id=%s ",id);
            number=stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public int tieCollectDel(String id, String phone) {
        Connection conn=null;
        Statement stat =null;
        String sql=null;
        int number=0;
        conn=util.getConnection();
        try {
            stat=conn.createStatement();
            sql=String.format("delete from tieCollectes where id=%s and phone='%s'",id,phone);
            number=stat.executeUpdate(sql);
            sql=String.format("update ties  set collect=collect-1 where id=%s ",id);
            number=stat.executeUpdate(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return number;
    }

    public List<Tie> getTieCollectByPhone(User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Tie> ties = null;
        ties = new ArrayList<Tie>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from ties,users,tiecollectes where ties.phone=users.phone and" +
                    " users.phone=tiecollectes.phone and " +
                    "ties.id=tiecollectes.id and tiecollectes.phone='%s'", phone);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Tie tie = creatTie(rs, phone);
                ties.add(tie);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return ties;
    }

    private Tie creatTie(ResultSet rs, String phone) {
        Tie tie = new Tie();
        try {
            tie.setId(rs.getInt("id"));
            tie.setPhone(rs.getString("phone"));
            tie.setTitle(rs.getString("title"));
            tie.setPraise(rs.getInt("praise"));
            tie.setCollect(rs.getInt("collect"));
            tie.setImg(rs.getString("img"));
            tie.setUsername(rs.getString("name"));
            tie.setPhoto(rs.getString("photo"));
            if (phone == null || phone.length() == 0) {
                tie.setPraiseFlag(false);
                tie.setCollectFlag(false);
            } else {
                tie.setPraiseFlag(getTiePraiseFlagByIdAndPhone(String.valueOf(tie.getId()), phone));
                tie.setCollectFlag(getTieCollectFlagByIdAndPhone(String.valueOf(tie.getId()), phone));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tie;
    }

    public List<Tie> getTies(User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        int number = 0;
        String phone = null;
        if (user != null) phone = user.getPhone();
        List<Tie> ties = null;
        ties = new ArrayList<Tie>();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from ties,users where ties.phone=users.phone");
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                Tie tie = creatTie(rs, phone);
                ties.add(tie);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return ties;
    }


    public Tie getTieById(String tieId, User user) {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = null;
        Tie tie = new Tie();
        String phone = null;
        if (user != null) phone = user.getPhone();
        conn = util.getConnection();
        try {
            stat = conn.createStatement();
            sql = String.format("select * from ties,users where ties.id=%s and ties.phone=users.phone", tieId);
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                tie = creatTie(rs, phone);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (conn != null) conn.close();
                if (stat != null) stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return tie;
    }

}
