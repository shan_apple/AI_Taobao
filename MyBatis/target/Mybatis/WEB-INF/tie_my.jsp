<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="cn.edu.lit.model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <title>美拍</title>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" />
    <link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>
<body>

<header id="header">
    <a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">美拍</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
</header>
<div class="column column-4 imgtxt-list">
    <div class="title">
        <span class="titletext fl"><i class="iconfont">&#xe603;</i>贴贴精选</span>
        <a href="All?method=tieListGo" class="more fr">更多<i class="iconfont">&#xe65f;</i></a>
    </div>
    <div class="con-list">
        <ul class="clearfix">
            <c:forEach var="tie" items="${tieAll }" >
                <li id="li${tie.id}">
                    <div class="li-box clearfix">
                        <a href="All?method=tieDetailGo&tieId=${tie.id}" class="img  fl"  onclick=""><img src="${tie.img}" ></a>
                        <div class="txt-box">
                            <p class="txt"><a href="All?method=tieDetailGo&tieId=${tie.id}" >${tie.title}</a></p>
                            <div class="info clearfix">
                                <a href="javascript:void(0)" onclick="tiePraise(${tie.id})">
                    <span class="zan">
                      <i class="iconfont" id="tiePraseIcon${tie.id}" style="color: ${tie.praiseColor}">&#xe600;</i>
                      <span id="tiePraseCount${tie.id}" class="zan">${tie.praise }</span>
                    </span></a>
                                <a href="javascript:void(0)" onclick="tieCollect(${tie.id})">
                    <span class="collect">
                      <i class="iconfont" id="tieCollectIcon${tie.id}" style="color: ${tie.collectColor}">&#xe605;</i>
                      <span id="tieCollectCount${tie.id}" class="collect">${tie.collect }</span>
                    </span>
                                </a>
                                <a style="color: red;font-size: 12px" href="javascript:void(0)" onclick="tieMineDel(${tie.id})"> 删除 </a>
                    <span class="author">
                      <img src="${tie.photo}">${tie.username }
                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
<%@ include file="foot.jsp" %>
<script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/layui/layui.js" charset="utf-8"></script>
<script >
    document.getElementById("foot3").classList.add("active");
    layui.use('layer', function(){ //独立版的layer无需执行这一句
        var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        //触发事件
        window.tiePraise=function(tieId){
            var user="${requestScope.user}";
            if(user==null||user.length==0||user=='null'){
                layer.confirm('是否先进行登录？', {
                    btn: ['登录','取消'] //按钮
                }, function(){
                    window.location.href="All?method=userLoginGo";
                }, function(){
                });
            }
            else {
                var praise=document.getElementById("tiePraseCount"+tieId);
                var icon=document.getElementById("tiePraseIcon"+tieId);
                $.post("All?method=tiePraise",
                    {
                        id:tieId
                    },
                    function(data,status){
                        var flagColor1=data.flagColor;
                        if(flagColor1=="0"){
                            icon.style.color="#646464";
                            praise.innerText=parseInt(praise.innerText)-1;
                        }
                        else if(flagColor1=="1"){
                            icon.style.color="#FF0000";
                            praise.innerText=parseInt(praise.innerText)+1;
                        }
                    }
                    ,"json"
                );
            }
        }
        window.tieCollect=function(tieId){
            var user="${requestScope.user}";
            if(user==null||user.length==0||user=='null'){
                layer.confirm('是否先进行登录？', {
                    btn: ['登录','取消'] //按钮
                }, function(){
                    window.location.href="All?method=userLoginGo";
                }, function(){
                });
            }
            else {
                var collect=document.getElementById("tieCollectCount"+tieId);
                var icon=document.getElementById("tieCollectIcon"+tieId);
                //646464  FF0000红


                $.post("All?method=tieCollect",
                    {
                        id:tieId
                    },
                    function(data,status){
                        var flagColor1=data.flagColor;
                        if(flagColor1=="0"){
                            icon.style.color="#646464";
                            collect.innerText=parseInt(collect.innerText)-1;
                        }
                        else if(flagColor1=="1"){
                            icon.style.color="#FF0000";
                            collect.innerText=parseInt(collect.innerText)+1;
                        }
                    }
                    ,"json"
                );
            }

        }

        window.tieMineDel=function(tieId){
            layer.confirm('是否删除美拍？', {
                btn: ['是','否'] //按钮
            }, function(){

                $.post("All?method=tieMainDel",
                    {
                        id:tieId
                    },
                    function(data,status){
                        var flag=data.flag;
                        if(flag=="1"){
                            $('#li'+meiId).hide();
                            layer.closeAll('dialog');
                        }

                    }
                    ,"json"
                );

            }, function(){
            });
        }

    });
</script>


</body>
</html>