package cn.edu.lit.util;

import cn.edu.config.MybatisConfig;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DbUtilC3P0 {
   static AnnotationConfigApplicationContext ctx= new AnnotationConfigApplicationContext(MybatisConfig.class);

    private static DataSource ds= null;
    static {
        ds = ctx.getBean("dataSource",DataSource.class);
    }
    public static Connection getConnection() {
        try {
            return ds.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    public static DataSource getDataSource() {
        return ds;
    }
}
