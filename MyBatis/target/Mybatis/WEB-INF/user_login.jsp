<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="icon" href="data:;base64,=">
<title>Insert title here</title>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="../resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="../resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../resources/layui/css/layui.css" media="all">


</head>

<body>

  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;"" class="iconfont fl">&#xe63f;</a>
    <div class="title">登录</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content">
  	 <div class="login-box">
  	 	 <div class="form-box">
  	 	 <form action="#" method="post" name="form1" >
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe608;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" value="${user.getPhone() }" placeholder="输入用户手机号"  id="userPhone" >
  	 	 		</div>

  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" placeholder="输入密码" id="userPassword">
  	 	 		</div>
  	 	 	</div>
  	 	 	 
  	 	 	<div class="h20"></div>  	 	 	
  	 	 	<div  class="row row-btn"  id="tip">
  	 	 	<a href = "javascript:void(0);"   class="green-btn">登录 </a>
  	 	 	</div>
  	 	 	</form>
  	 	 	<div class="row row-btn" >
  	 	 		<a href="All?method=userRegister" class="boder-btn"  >加入会员</a>
  	 	 	</div>
  	 	 	<div class="row row-txt">
  	 	 		<a href="All?method=userForgetPasswordGo" class="forget">忘记密码</a>
  	 	 	</div>
  	 	 </div>
  	 	 <div class="h20"></div>
  	 	 <div class="otherway">
  	 	 	 <div class="title"><span>其他登录方式</span></div>
  	 	 	 <ul class="clearfix">
  	 	 	 	<li>
  	 	 	 		<a href="#" class="weibo"><i class="iconfont">&#xe60a;</i></a>
  	 	 	 	</li>
  	 	 	 	<li>
  	 	 	 		<a href="#" class="qq"><i class="iconfont">&#xe607;</i></a>
  	 	 	 	</li>
  	 	 	 	<li>
  	 	 	 		<a href="#" class="weixin"><i class="iconfont">&#xe606;</i></a>
  	 	 	 	</li>
  	 	 	 </ul>
  	 	 </div>
  	 </div>
  </div>
  <%@ include file="foot.jsp" %>



<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的

-->
  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <script src="../resources/layui/layui.js" charset="utf-8"></script>
  <script >
	  document.getElementById("foot4").classList.add("active");
	layui.use('layer', function(){ //独立版的layer无需执行这一句
		var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		var userPhone;
		var userPassword;
		//触发事件
		var active = {
			tip1:function () {
				layer.msg("手机号填写错误", {
					//time: 3000, //3s后自动关闭
					offset: 't',//设置距离 上,左 位置['40%','30%']
					anim: 6//出现方式为抖动
				});
			}
			,
			tip2:function () {
				layer.msg("密码填写错误", {
					//time: 3000, //3s后自动关闭
					offset: 't',//设置距离 上,左 位置['40%','30%']
					anim: 6//出现方式为抖动
				});
			}
			,
			tip3:function () {
				layer.msg("不存在该用户", {
					//time: 3000, //3s后自动关闭
					offset: 't',//设置距离 上,左 位置['40%','30%']
					anim: 6//出现方式为抖动
				});
			}
			,
			tip4:function () {
				layer.msg("登录成功", {
					icon:16,
					time: 1000, //3s后自动关闭
					offset: 't',//设置距离 上,左 位置['40%','30%']
					anim: 5//出现方式为抖动
				});
				var back="${requestScope.back}";
				if(back==null||back.length==0||back=='null'){
					self.location=document.referrer;
				}
				window.location.href="/All?method=userIndexGo";

				//window.location.href="/All?method=indexGo";
				//window.history.back(-1);
			}
			,
			confirmTrans: function(){
				layer.msg('这是一个半透明提示框', {
					time: 3000, //3s后自动关闭
					offset: 't',//设置距离 上,左 位置['40%','30%']
					anim: 6//出现方式为抖动
					//area:['80px','66px']//设置长宽度
					//btn: ['明白了', '知道了', '哦']
				});
			}
		};



		$("#tip").on('click', function(){
			var othis = $(this), method = "";
			userPhone=document.getElementById("userPhone").value;
			userPassword=document.getElementById("userPassword").value;
			//isNaN(userPhone)判断是否全为数字
			if(userPhone==null||userPhone.length!=11||isNaN(userPhone)){
				method="tip1";
				active[method] ? active[method].call(this, othis) : '';
				return ;
			}
			if(userPassword==null||userPassword.length<6){
				method="tip2";
				active[method] ? active[method].call(this, othis) : '';
				return;
			};
			//$.post( url, [data], [callback], [type] )
			//        路径 ,{"name":"juon"},function(){},"json"

			$.post("All?method=userLogin",
					{
						userPhone:userPhone,
						userPassword:userPassword
					},
					function(data,status){
						var number=data.praiseflag;
						if(number=="0"){
							method="tip3";//不存在
							active[method] ? active[method].call(this, othis) : '';
						}
						else if(number=="1"){
							method="tip2";//密码错误
							active[method] ? active[method].call(this, othis) : '';
						}
						if(number=="2"){
							method="tip4";//登陆成功
							active[method] ? active[method].call(this, othis) : '';
						}
						//alert("数据: \n" + data.praiseflag + "\n状态: " + status);
						}
						,"json"
			);

			//var othis = $(this), method = othis.data('method');
			//active[method] ? active[method].call(this, othis) : '';
		});
	});
</script>
</body>
</html>