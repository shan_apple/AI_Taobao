<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">修改</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content">
  	 <div class="register-box">
  	 	 <div class="form-box">	  
         <form name="form1" action="All?method=userInfoUpdateDescriptionDo" method="post">
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" placeholder="简介" name="discription" value="${valuedis}">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-btn">
  	 	 		<a href="javascript:document.form1.submit();"class="green-btn">确定</a>
  	 	 	</div>
  	 	 	</form>
  	 	 </div>
  	 </div>
     
  </div>
 
  <%@ include file="foot.jsp" %>
</body>
</html>
