<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="cn.edu.lit.model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">会员中心</div>
    <!-- <a href="#" class="iconfont fr">&#xe6a0;</a> -->
  </header> 
  <div class="content u-index">
  	  <div class="userbox">
  	  	  <div class="userbox-1">
  	  	  	<div class="userimg"><img src="${user.photo }"></div>
  	 		<div class="username">${user.name }</div>
  	  	  </div>
  	  	  <div class="userbox-2">
  	  	  	  <ul class="clearfix">
  	  	  	  	<li><a href="All?method=userTixianGo" class="c000"><span>¥${user.balance}</span><br>钱包余额</a></li>
  	  	  	  	<li><span>¥${user.cash}</span><br>已提现额度</li>
  	  	  	  </ul>
  	  	  </div>
  	  </div>
  	  <div class="ul-list">
  	  	   <ul>
  	  	   	  <li class="tel">
  	  	   	  	  <i class="iconfont">&#xe68e;</i>${user.phone}
  	  	   	  	  <span class="fr">显示在微名片<input type="checkbox" class="switch" checked></span>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="All?method=userInfoGo" class="arrow-right">
  	  	   	  	  <i class="iconfont">&#xe660;</i>个人信息
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="#" class="arrow-right">
  	  	   	  	  <i class="iconfont">&#xe625;</i>我的团队
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="All?method=userCollectGo" class="arrow-right">
  	  	   	  	  <i class="iconfont">&#xe73a;</i>我的收藏
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="#" class="arrow-right">
  	  	   	  	  <i class="iconfont">&#xe604;</i>一键生成二维码
  	  	   	  	</a>
  	  	   	  </li>
  	  	   </ul>
           
           
  	  </div>
             <div class="ul-list">
  	  	   <ul>
  	  	   	  <li id="logout">
  	  	   	  	<a href="javascript:void(0);" style="text-align:center; color:#999;">退 出
  	  	   	  	</a>
  	  	   	  </li>
  	  	   </ul>
  	   </div>
  </div>
 
  <%@ include file="foot.jsp" %>
  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <script src="resources/layui/layui.js" charset="utf-8"></script>
  <script >
	  document.getElementById("foot4").classList.add("active");
	  layui.use('layer', function(){ //独立版的layer无需执行这一句
		  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		  $("#logout").on('click', function (message){
			  layer.confirm('是否退出登录？', {
				  btn: ['退出','取消'] //按钮
			  }, function(){
				  $.post("All?method=userLogout",
						  function(data,status){
							  var number=data.flag;
							  if(number=="1"){
								  window.location.href="All?method=userIndexGo";
							  }
						  }
						  ,"json"
				  );
			  }, function(){
			  });
		  });
	  });
  </script>

</body>
</html>