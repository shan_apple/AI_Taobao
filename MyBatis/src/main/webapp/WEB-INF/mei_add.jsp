<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>微风向</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="resources/layui/css/layui.css"  media="all">
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body style="background:#fff;">
<header id="header">
	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
	<div class="title">美拍</div>
	<a href="#" class="iconfont fr">&#xe6a0;</a>
</header>
<form action="/All?method=meiAddDo"  name="form1" method="post" enctype="multipart/form-data">
	<div class="content mei_edit">
		<div class="detail_box">
			<div class="con">
				<div class="row-r1">
					<input class="input_title" placeholder="请输入标题（必填）" name="title" id="title">
				</div>
			</div>

			<div class="row-r1">
				<input type="file" class="input_title"  name="uploadFile"  onchange="selectImage(this);" border="0" id="img">
			</div>
			<div class="row-r1">
				<img id="image" style="width: 98%;" src=""/>
			</div>
		</div>
	</div>
	<div class=" clearfix"></div>
	</div>
</form>
<footer id="footer" class="foot"  >
	<a href="javascript:void(0)" class="green-btn ft-btn" onclick="sub()"> 发布</a>
</footer>


<script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/layui/layui.js" charset="utf-8"></script>
<script >

	layui.use('layer', function(){ //独立版的layer无需执行这一句
		var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		//触发事件
		function success () {
			layer.msg("发布成功", {
				icon:1,
				offset: 't',//设置距离 上,左 位置['40%','30%']
				anim: 5//出现方式为渐显
			});
			document.form1.submit();

		}
		function error (str) {
			layer.msg(str, {
				offset: 't',//设置距离 上,左 位置['40%','30%']
				anim: 6//出现方式为抖动
			});
		}


		window.sub=function () {
			var title=document.getElementById('title').value;
			var img=document.getElementById('img').value;
			if(title.length==0||title==null){
				error('标题不可为空')
				return;
			}
			if(img==null||img.length==0){
				error('图片不可为空')
				return;
			}
			success()



		}





	});
</script>

<script>
	var image = '';
	function selectImage(file) {
		if (!file.files || !file.files[0]) {
			return;
		}
		var reader = new FileReader();
		reader.onload = function (evt) {
			document.getElementById('image').src = evt.target.result;
			image = evt.target.result;
		}
		reader.readAsDataURL(file.files[0]);
	}
</script>
</body>
</html>
