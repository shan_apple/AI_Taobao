<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">个人资料</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content">
  	  <div class="ul-list">
  	  	 <ul>
  	  	 	<li class="pic">
  	  	 		<a>头像
  	  	 		   <span class="fr"><img src="${user.photo}"></span>
  	  	   	  	</a>
  	  	 	</li>
  	  	 </ul>
  	  </div>
  	  <div class="ul-list">
  	  	   <ul>
  	  	   	  <li>
  	  	   	  	<a href="All?method=userInfoUpdateNameGo" class="arrow-right">昵称
                   <span class="fr">${user.getName()}</span>
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="All?method=userInfoUpdateSexGo" class="arrow-right">性别
                   <span class="fr">${ user.getSex()}</span>
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	  <li>
  	  	   	  	<a href="" class="arrow-right">电话
                    <span class="fr">${user.getPhone()}</span>
  	  	   	  	</a>
  	  	   	  </li>
  	  	   	 
  	  	   </ul>
  	  </div>
  	  <div class="ul-list">
  	  	   <ul>
  	  	   	  <li>
  	  	   	  	<a href="All?method=userInfoUpdateDescriptionGo" class="arrow-right">个性签名
  	  	   	  		<span class="fr">${user.getDescription() }</span>
  	  	   	  	</a>
  	  	   	  </li>
  	  	   </ul>
  	   </div>
  </div>
 
  <%@ include file="foot.jsp" %>
</body>
</html>