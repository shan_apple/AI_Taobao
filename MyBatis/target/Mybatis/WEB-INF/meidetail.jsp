<%@ page language="java" contentType="text/html; charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="cn.edu.lit.model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <title>美拍</title>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<header id="header">
    <a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">美颜详情</div>

</header>
<div class="content con-detail">
    <div class="detail_box">
        <div class="con">
            <p class="title">${mei.title}</p>
            <p class="pic"><img src="${mei.img}"><i class="icon"><img src="${framer.photo}"></i></p>
        </div>
        <div class="info clearfix">
            <a href="javascript:void(0)" onclick="">
                    <span class="author">
                      <img src="${mei.photo}">
                      <span class="author">${mei.username }</span>
                    </span>
            </a>
            <a href="javascript:void(0)" onclick="meiPraise(${mei.id})">
                    <span class="zan">
                      <i class="iconfont" id="meiPraseIcon${mei.id}" style="color: ${mei.praiseColor}">&#xe600;</i>
                      <span class="zan" id="meiPraseCount${mei.id}">${mei.praise }</span>
                    </span></a>
            <a href="javascript:void(0)" onclick="meiCollect(${mei.id})">
                    <span class="collect">
                      <i class="iconfont" id="meiCollectIcon${mei.id}" style="color: ${mei.collectColor}">&#xe605;</i>
                      <span class="collect" id="meiCollectCount${mei.id}">${mei.collect }</span>
                    </span>
            </a>
        </div>
    </div>

    <div class="user_card clearfix">
        <i class="icon icon-left"></i>
        <i class="icon icon-right"></i>
        <div class="item pic">
            <div class="userimg"><img src="${mei.photo}"></div>
            <div class="username">${frame.name}</div>
        </div>
        <div class="item info">
            <div class="tel">电话：${framer.phone}</div>
            <div class="signature">个性签名：</div>
            <p class="signature_txt">${framer.description}</p>
        </div>
        <div class="item code">
            <div class="codeimg"><img src="resources/images/code.png"></div>
        </div>
    </div>
</div>

<%@ include file="foot.jsp" %>

<script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/layui/layui.js" charset="utf-8"></script>
<script>
    document.getElementById("foot1").classList.add("active");


    layui.use('layer', function () { //独立版的layer无需执行这一句
        var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        //触发事件
        function success() {
            layer.msg("注册成功", {
                icon: 1,
                offset: 't',//设置距离 上,左 位置['40%','30%']
                anim: 5//出现方式为渐显
            });
        }

        function error(str) {
            layer.msg(str, {
                offset: 't',//设置距离 上,左 位置['40%','30%']
                anim: 6//出现方式为抖动
            });
        }


        window.meiPraise = function (meiId) {
            var user = "${requestScope.user}";
            if (user == null || user.length == 0 || user == 'null') {
                layer.confirm('是否先进行登录？', {
                    btn: ['登录', '取消'] //按钮
                }, function () {
                    window.location.href = "All?method=userLoginGo";
                }, function () {
                });
            } else {
                var praise = document.getElementById("meiPraseCount" + meiId);
                var icon = document.getElementById("meiPraseIcon" + meiId);
                //646464  FF0000红


                $.post("All?method=meiPraise",
                    {
                        id: meiId
                    },
                    function (data, status) {
                        var flagColor1 = data.flagColor;
                        if (flagColor1 == "0") {
                            icon.style.color = "#646464";
                            praise.innerText = parseInt(praise.innerText) - 1;
                        } else if (flagColor1 == "1") {
                            icon.style.color = "#FF0000";
                            praise.innerText = parseInt(praise.innerText) + 1;
                        }
                    }
                    , "json"
                );
            }


        }
        window.meiCollect = function () {

        }


    });
</script>

</body>
</html>

