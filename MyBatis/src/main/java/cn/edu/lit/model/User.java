package cn.edu.lit.model;

public class User {

    private String phone;
    private String password;
    private String name;
    private String sex;
    private String description;
    private String photo;
    private int balance;
    private int cash;

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoto() {
        return photo;
    }

    public int getBalance() {
        return balance;
    }

    public int getCash() {
        return cash;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setDescription(String discription) {
        this.description = discription;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public User(String phone, String name, String password, String sex) {
        super();
        this.phone = phone;
        this.name = name;
        this.password = password;
        this.sex = sex;
    }




    



    public User(String phone, String password) {
        super();
        this.phone = phone;
        this.password = password;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", discription='" + description + '\'' +
                ", photo='" + photo + '\'' +
                ", balance=" + balance +
                ", cash=" + cash +
                '}';
    }
}

