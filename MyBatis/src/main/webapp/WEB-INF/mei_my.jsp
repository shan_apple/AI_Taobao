<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<%@ page import="cn.edu.lit.model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>
<body>

  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">美拍</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content mei_list">  	
  	<div class="card-list">
  		<div class="con-list">
  	 		<ul class="clearfix">

				<c:forEach var="mei" items="${meiAll }">
					<li id="li${mei.id}">
						<div class="photo-card">
							<a href="All?method=meiDetailGo&meiId=${mei.id}" class="img"  onclick=""><img src="${mei.img}" ></a>
							<p class="txt"><a href="" >${mei.title}</a></p>
							<div class="info clearfix">
								<a href="javascript:void(0)" onclick="">
                    <span class="author">
                      <img src="${mei.photo}">
                      <span class="author">${mei.username }</span>
                    </span>
								</a>
								<a href="javascript:void(0)" onclick="meiPraise(${mei.id})">
                    <span class="zan">
                      <i class="iconfont" id="meiPraseIcon${mei.id}" style="color: ${mei.praiseColor}">&#xe600;</i>
                      <span class="zan" id="meiPraseCount${mei.id}">${mei.praise }</span>
                    </span></a>
								<a href="javascript:void(0)" onclick="meiCollect(${mei.id})">
                    <span class="collect">
                      <i class="iconfont" id="meiCollectIcon${mei.id}" style="color: ${mei.collectColor}">&#xe605;</i>
                      <span class="collect" id="meiCollectCount${mei.id}">${mei.collect }</span>
                    </span>
								</a>
								<a style="color: red;font-size: 12px" href="javascript:void(0)" onclick="meiMineDel(${mei.id})"> 删除 </a>
							</div>
						</div>
					</li>
				</c:forEach>
  	 			
  	 		</ul>
  	 	</div>
  	</div>
  </div>
 <%@ include file="foot.jsp" %>
  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <script src="resources/layui/layui.js" charset="utf-8"></script>
  <script >
	  document.getElementById("foot3").classList.add("active");
	  layui.use('layer', function(){ //独立版的layer无需执行这一句
		  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		  //触发事件
		  window.meiPraise=function(meiId){
			  var user="${requestScope.user}";
			  if(user==null||user.length==0||user=='null'){
				  layer.confirm('是否先进行登录？', {
					  btn: ['登录','取消'] //按钮
				  }, function(){
					  window.location.href="All?method=userLoginGo";
				  }, function(){
				  });
			  }
			  else {
				  var praise=document.getElementById("meiPraseCount"+meiId);
				  var icon=document.getElementById("meiPraseIcon"+meiId);
				  $.post("All?method=meiPraise",
						  {
							  id:meiId
						  },
						  function(data,status){
							  var flagColor1=data.flagColor;
							  if(flagColor1=="0"){
								  icon.style.color="#646464";
								  praise.innerText=parseInt(praise.innerText)-1;
							  }
							  else if(flagColor1=="1"){
								  icon.style.color="#FF0000";
								  praise.innerText=parseInt(praise.innerText)+1;
							  }
						  }
						  ,"json"
				  );
			  }
		  }
		  window.meiCollect=function(meiId){
			  var user="${requestScope.user}";
			  if(user==null||user.length==0||user=='null'){
				  layer.confirm('是否先进行登录？', {
					  btn: ['登录','取消'] //按钮
				  }, function(){
					  window.location.href="All?method=userLoginGo";
				  }, function(){
				  });
			  }
			  else {
				  var collect=document.getElementById("meiCollectCount"+meiId);
				  var icon=document.getElementById("meiCollectIcon"+meiId);
				  //646464  FF0000红


				  $.post("All?method=meiCollect",
						  {
							  id:meiId
						  },
						  function(data,status){
							  var flagColor1=data.flagColor;
							  if(flagColor1=="0"){
								  icon.style.color="#646464";
								  collect.innerText=parseInt(collect.innerText)-1;
							  }
							  else if(flagColor1=="1"){
								  icon.style.color="#FF0000";
								  collect.innerText=parseInt(collect.innerText)+1;
							  }
						  }
						  ,"json"
				  );
			  }

		  }

		  window.meiMineDel=function(meiId){
			  layer.confirm('是否删除美拍？', {
				  btn: ['是','否'] //按钮
			  }, function(){
				   $.post("All?method=meiMineDel",
						  {
							  id:meiId
						  },
						  function(data,status){
							  var flag=data.flag;
							  if(flag=="1"){
								$('#li'+meiId).hide();
								layer.closeAll('dialog');
							  }

						  }
						  ,"json"
				  );


			  }, function(){
			  });

		  }

	  });
  </script>


</body>
</html>