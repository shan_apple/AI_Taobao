<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="cn.edu.lit.*"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
  <title>美拍</title>
  <link href="../resources/css/style.css" rel="stylesheet" type="text/css" />
  <link href="../resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>

<body>
<header id="header">
  <a  class="iconfont fl"></a>
  <div class="title">美拍首页</div>
  <a  class="iconfont fr"></a>
</header>
<div class="content">
    <C:if test="${requestScope.user!=null}">
      <div class="user_card clearfix">
        <div class="item pic">
          <div class="userimg"><img src="${user.photo}"></div>
          <div class="username">${user.getName()}</div>
        </div>
        <div class="item info">
          <div class="tel">电话：${user.getPhone()}</div>
          <div class="signature">个性签名：</div>
          <p class="signature_txt">${user.getDescription()}</p>
        </div>
        <div class="item code">
          <div class="codeimg"><img src="../resources/images/code.png"></div>
        </div>
      </div>
        <div class="Edit">
            <a href="All?method=meiImageSearchGo" class="edit-btn"><i class="iconfont">&#xe62e;</i>我要搜</a>
        </div>
        <div class="Edit">
            <a href="javascript:void(0)"> </a>
        </div>
    </C:if>






















    <div class="column column-1 card-list">
      <div class="title">
        <span class="titletext fl"><i class="iconfont">&#xe62e;</i>推荐</span>
        <a href="All?method=meiListGo" class="more fr">更多<i class="iconfont">&#xe65f;</i></a>
      </div>
      <div class="con-list">
        <ul class="clearfix">
        <c:forEach var="mei" items="${meiAll }" >
          <li >
            <div class="photo-card">
              <a href="All?method=meiDetailGo&meiId=${mei.id}" class="img"  onclick=""><img src="${mei.img}" ></a>
              <p class="txt">${mei.title}</p>
              <div class="info clearfix">

                    <span class="author">
                      <img src="${mei.photo}">
                      <span class="author">${mei.username }</span>
                    </span>
                  </a>
                  <a href="javascript:void(0)" onclick="meiPraise(${mei.id})">
                    <span class="zan">
                      <i class="iconfont" id="meiPraseIcon${mei.id}" style="color: ${mei.praiseColor}">&#xe600;</i>
                      <span class="zan" id="meiPraseCount${mei.id}">${mei.praise }</span>
                    </span></a>
                  <a href="javascript:void(0)" onclick="meiCollect(${mei.id})">
                    <span class="collect">
                      <i class="iconfont" id="meiCollectIcon${mei.id}" style="color: ${mei.collectColor}">&#xe605;</i>
                      <span class="collect" id="meiCollectCount${mei.id}">${mei.collect }</span>
                    </span>
                  </a>
              </div>
            </div>
          </li>
        </c:forEach>
        </ul>
      </div>
    </div>





  <%--<div class="column column-4 imgtxt-list">
    <div class="title">
      <span class="titletext fl"><i class="iconfont">&#xe603;</i>贴贴精选</span>
      <a href="All?method=tieDiyGo" class="more fr">更多<i class="iconfont">&#xe65f;</i></a>
    </div>
    <div class="con-list">
      <ul class="clearfix">
        <c:forEach var="tie" items="${tieAll }" end="5">
          <li >
            <div class="li-box clearfix">
              <a href="All?method=tieDetailGo&tieId=${tie.id}" class="img  fl"  onclick=""><img src="${tie.img}" ></a>
              <div class="txt-box">
                <p class="txt"><a href="All?method=tieDetailGo&tieId=${tie.id}" >${tie.title}</a></p>
                <div class="info clearfix">
                  <a href="javascript:void(0)" onclick="tiePraise(${tie.id})">
                    <span class="zan">
                      <i class="iconfont" id="tiePraseIcon${tie.id}" style="color: ${tie.praiseColor}">&#xe600;</i>
                      <aaa id="tiePraseCount${tie.id}">${tie.praise }</aaa>
                    </span></a>
                  <a href="javascript:void(0)" onclick="tieCollect(${tie.id})">
                    <span class="collect">
                      <i class="iconfont" id="tieCollectIcon${tie.id}" style="color: ${tie.collectColor}">&#xe605;</i>
                      <aaa id="tieCollectCount${tie.id}">${tie.collect }</aaa>
                    </span>
                  </a>
                  <a href="javascript:void(0)" onclick="">
                    <span class="author">
                      <img src="${tie.photo}">${tie.username }
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </li>
        </c:forEach>
      </ul>
    </div>
  </div>--%>

</div>


<div id="go-top" class="go-top">
  <a href="javascript:goTop();" class="iconfont">&#xe61012345648789788;</a>
</div>
<%@ include file="foot.jsp" %>
<script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
<script src="resources/layui/layui.js" charset="utf-8"></script>
<script >
  document.getElementById("foot1").classList.add("active");



  layui.use('layer', function(){ //独立版的layer无需执行这一句
    var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
    //触发事件
    function success () {
      layer.msg("注册成功", {
        icon:1,
        offset: 't',//设置距离 上,左 位置['40%','30%']
        anim: 5//出现方式为渐显
      });
    }
    function error (str) {
      layer.msg(str, {
        offset: 't',//设置距离 上,左 位置['40%','30%']
        anim: 6//出现方式为抖动
      });
    }

    

    window.meiPraise=function(meiId){
      var user="${requestScope.user}";
      if(user==null||user.length==0||user=='null'){
        layer.confirm('是否先进行登录？', {
          btn: ['登录','取消'] //按钮
        }, function(){
          window.location.href="All?method=userLoginGo";
        }, function(){
        });
      }
      else {
        var praise=document.getElementById("meiPraseCount"+meiId);
        var icon=document.getElementById("meiPraseIcon"+meiId);
        //646464  FF0000红


        $.post("All?method=meiPraise",
                {
                  id:meiId
                },
                function(data,status){
                  var flagColor1=data.flagColor;
                  if(flagColor1=="0"){
                    icon.style.color="#646464";
                    praise.innerText=parseInt(praise.innerText)-1;
                  }
                  else if(flagColor1=="1"){
                    icon.style.color="#FF0000";
                    praise.innerText=parseInt(praise.innerText)+1;
                  }
                }
                ,"json"
        );
      }



    }
    window.meiCollect=function(meiId){
      var user="${requestScope.user}";
      if(user==null||user.length==0||user=='null'){
        layer.confirm('是否先进行登录？', {
          btn: ['登录','取消'] //按钮
        }, function(){
          window.location.href="All?method=userLoginGo";
        }, function(){
        });
      }
      else {
        var collect=document.getElementById("meiCollectCount"+meiId);
        var icon=document.getElementById("meiCollectIcon"+meiId);
        //646464  FF0000红


        $.post("All?method=meiCollect",
                {
                  id:meiId
                },
                function(data,status){
                  var flagColor1=data.flagColor;
                  if(flagColor1=="0"){
                    icon.style.color="#646464";
                    collect.innerText=parseInt(collect.innerText)-1;
                  }
                  else if(flagColor1=="1"){
                    icon.style.color="#FF0000";
                    collect.innerText=parseInt(collect.innerText)+1;
                  }
                }
                ,"json"
        );
      }

    }
    

    //#################################################
    <!--region tei  collect and praise-->
    window.tiePraise=function(tieId){
      var user="${requestScope.user}";
      if(user==null||user.length==0||user=='null'){
        layer.confirm('是否先进行登录？', {
          btn: ['登录','取消'] //按钮
        }, function(){
          window.location.href="All?method=userLoginGo";
        }, function(){
        });
      }
      else {
        var praise=document.getElementById("tiePraseCount"+tieId);
        var icon=document.getElementById("tiePraseIcon"+tieId);
        //646464  FF0000红


        $.post("All?method=tiePraise",
                {
                  id:tieId
                },
                function(data,status){
                  var flagColor1=data.flagColor;
                  if(flagColor1=="0"){
                    icon.style.color="#646464";
                    praise.innerText=parseInt(praise.innerText)-1;
                  }
                  else if(flagColor1=="1"){
                    icon.style.color="#FF0000";
                    praise.innerText=parseInt(praise.innerText)+1;
                  }
                }
                ,"json"
        );
      }



    }
    window.tieCollect=function(tieId){
      var user="${requestScope.user}";
      if(user==null||user.length==0||user=='null'){
        layer.confirm('是否先进行登录？', {
          btn: ['登录','取消'] //按钮
        }, function(){
          window.location.href="All?method=userLoginGo";
        }, function(){
        });
      }
      else {
        var collect=document.getElementById("tieCollectCount"+tieId);
        var icon=document.getElementById("tieCollectIcon"+tieId);
        //646464  FF0000红


        $.post("All?method=tieCollect",
                {
                  id:tieId
                },
                function(data,status){
                  var flagColor1=data.flagColor;
                  if(flagColor1=="0"){
                    icon.style.color="#646464";
                    collect.innerText=parseInt(collect.innerText)-1;
                  }
                  else if(flagColor1=="1"){
                    icon.style.color="#FF0000";
                    collect.innerText=parseInt(collect.innerText)+1;
                  }
                }
                ,"json"
        );
      }

    }
    <!--endregion-->




  });
</script>


</body>

<!--回到顶部-->
<script type="text/javascript">
  document.getElementById("foot1").classList.add("active");
  var $top = $('#go-top');
  $(window).bind('scroll',function(){
    var $this = $(this);
    if($this.scrollTop() > 360){
      $top.show();
    }else{
      $top.hide();
    }
  })
  function goTop(){
    $('html,body').animate({
      scrollTop:0
    },400)
  }
</script>

</html>
