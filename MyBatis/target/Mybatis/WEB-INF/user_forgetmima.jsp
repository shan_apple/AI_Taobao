<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">找回密码</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content">
  <form action="userforgetmima.do" name="form1">
  	 <div class="register-box">
  	 	 <div class="form-box">
  	 	 
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe64f;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" placeholder="手机号" name="phone" style="width:200px;">
                    <a href="" class="yzm fr">发送验证码</a>
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" placeholder="请输入验证码">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box" placeholder="新密码" name="password">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-btn">
  	 	 		<a href="javascript:document.form1.submit();" class="green-btn">确定</a>
  	 	 	</div>
  	 	 </div>
  	 </div>
  	 </form>
     
  </div>
 
  <%@ include file="foot.jsp" %>
</body>
</html>
