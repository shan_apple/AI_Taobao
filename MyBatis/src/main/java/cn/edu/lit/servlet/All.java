package cn.edu.lit.servlet;


import cn.edu.lit.common.Constants;
import cn.edu.lit.dal.Dal;
import cn.edu.lit.model.Mei;
import cn.edu.lit.model.Tie;
import cn.edu.lit.model.User;
import com.baidu.aip.imagesearch.AipImageSearch;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@WebServlet(name = "All",urlPatterns = "/All")
public class All extends HttpServlet {
    //设置APPID/AK/SK
    public static final String APP_ID = "23900832";
    public static final String API_KEY = "9Kqjuyn2F8HVzzqygdNynj41";
    public static final String SECRET_KEY = "yMVVMKKpWlOP4mdj5bbILXqY3GLKyLNH";
    // 初始化一个AipImageSearch
    AipImageSearch client = new AipImageSearch(APP_ID, API_KEY, SECRET_KEY);


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf8");
        String method=request.getParameter("method");

        switch(method){
            case "userLogin":userLogin(request,response);break;
            case "userLoginGo":userLoginGo(request,response);break;
            case "userLogout":userLogout(request,response);break;
            case "userRegister":userRegisterGo(request,response);break;
            case "userRegisterDo":userRegisterDo(request,response);break;
            case "userProcotolGo":userProcotolGo(request,response);break;
            case "userIndexGo":userIndexGo(request,response);break;
            case "userInfoGo":userInfoGo(request,response);break;
            case "userInfoUpdateNameGo":userInfoUpdateNameGo(request,response);break;
            case "userInfoUpdateNameDo":userInfoUpdateNameDo(request,response);break;
            case "userInfoUpdateSexGo":userInfoUpdateSexGo(request,response);break;
            case "userInfoUpdateSexDo":userInfoUpdateSexDo(request,response);break;
            case "userInfoUpdateDescriptionGo":userInfoUpdateDescriptionGo(request,response);break;
            case "userInfoUpdateDescriptionDo":userInfoUpdateDescriptionDo(request,response);break;
            case "userCollectGo":userCollectGo(request,response);break;
            case "userTixianGo":userTixianGo(request,response);break;
            case "userTixianDo":userTixianDo(request,response);break;
            case "userForgetPasswordGo":userForgetPasswordGo(request,response);break;
            case "userForgetPasswordDo":userForgetPasswordDo(request,response);break;
            case "indexGo":indexGo(request,response);break;
            
            
            case "meiDiyGo":meiDiyGo(request,response);break;
            case "meiListGo":meiListGo(request,response);break;
            case "meiPraise":meiPraise(request,response);break;
            case "meiCollect":meiCollect(request,response);break;            
            case "meiAddGo":meiAddGo(request,response);break;
            case "meiAddDo":meiAddDo(request,response);break;
            case "meiDetailGo":meiDetailGo(request,response);break;            
            case "meiMineGo":meiMineGo(request,response);break;
            case "meiMineDel":meiMineDel(request,response);break;
            case "meiImageSearchGo":meiImageSearchGo(request,response);break;
            case "meiImageSearchDo":meiImageSearchDo(request,response);break;

            case "tieDiyGo":tieDiyGo(request,response);break;            
            case "tiePraise":tiePraise(request,response);break;
            case "tieCollect":tieCollect(request,response);break;
            case "tieAddGo":tieAddGo(request,response);break;
            case "tieAddDo":tieAddDo(request,response);break;
            case "tieDetailGo":tieDetailGo(request,response);break;
            case "tieMineGo":tieMineGo(request,response);break;
            case "tieMineDel":tieMineDel(request,response);break;

        }


    }

    //region About Mei
    protected void meiMineDel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User) request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String meiId=request.getParameter("id");
        Dal dal=new Dal();
        String meiContSign=dal.getContSignById(meiId);
        dal.meiMineDel(meiId);

        String contSign=meiContSign;
        HashMap<String, String> options = new HashMap<String, String>();
        JSONObject res = client.productDeleteBySign(contSign, options);
        System.out.println(res.toString(2));



        response.getWriter().print("{\"flag\":1}");

    }

    protected void meiMineGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=null;
        user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        else {
            Dal dal=new Dal();
            List <Mei> meis=null;
            meis=dal.getMeiMineList(user);
            request.setAttribute("meiAll",meis);
            request.getRequestDispatcher("WEB-INF/mei_my.jsp").forward(request, response);
        }
    }



    protected void meiDetailGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute(Constants.CURRENT_USER);
        request.setAttribute("user", user);
        String meiId=request.getParameter("meiId");
        Dal dal=new Dal();
        Mei mei=dal.getMeiById(meiId,user);
        String phone= mei.getPhone();
        User framer=dal.getUserByPhone(phone);
        request.setAttribute("framer", framer);
        request.setAttribute("mei", mei);
        request.getRequestDispatcher("WEB-INF/meidetail.jsp").forward(request, response);
    }

    protected void meiImageSearchDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf8");
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) {
            response.sendRedirect("All?method=userLoginGo");
            return;
        }
        String titleSearch = "";

        String filePath="";
        titleSearch=request.getParameter("titleSearch");
        System.out.println("titlesearch"+titleSearch);

        // 上传文件存储目录
        String UPLOAD_DIRECTORY = "resources";
        // 上传配置
        int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
        int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
        int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB



        // 检测是否为多媒体上传

        if (!ServletFileUpload.isMultipartContent(request)) {
            // 如果不是则停止
            PrintWriter writer = response.getWriter();
            writer.println("Error: 表单必须包含 enctype=multipart/form-data");
            writer.flush();
            return;
        }

        // 配置上传参数
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // 设置内存临界值 - 超过后将产生临时文件并存储于临时目录中
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // 设置临时存储目录
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        // 设置最大文件上传值
        upload.setFileSizeMax(MAX_FILE_SIZE);

        // 设置最大请求值 (包含文件和表单数据)
        upload.setSizeMax(MAX_REQUEST_SIZE);

        // 中文处理
        upload.setHeaderEncoding("UTF-8");

        // 构造临时路径来存储上传的文件
        // 这个路径相对当前应用的目录

        String uploadPath = getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY+File.separator +"images";
        String fileName=null;

        // 如果目录不存在则创建
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            // 解析请求的内容提取文件数据
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);

            if (formItems != null && formItems.size() > 0) {
                // 迭代表单数据
                for (FileItem item : formItems) {
                    // 处理不在表单中的字段
                    if (!item.isFormField()) {

                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
                        //System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
                        fileName = df.format(new Date())+new File(item.getName()).getName();
                        filePath = uploadPath + File.separator + fileName;
                        //imagePath+=File.separator + fileName;
                        File storeFile = new File(filePath);
                        // 在控制台输出文件的上传路径
                        System.out.println(filePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        request.setAttribute("message",
                                "文件上传成功!");
                    }
                    else {
                        if ("title".equals(item.getFieldName())) {
                           // title=item.getString("UTF-8");
                        }
                    }
                }
            }
        } catch (Exception ex) {
            request.setAttribute("message",
                    "错误信息: " + ex.getMessage());
        }






        System.out.println(this.getClass().getClassLoader().getResource(""));
        String path = filePath;
        JSONObject res = client.productSearch(path, new HashMap<String, String>());
        JSONArray result=res.getJSONArray("result");
        List<String> contsigns=new ArrayList<String>();
        for (int i=0;i<result.length();i++){
            System.out.println(result.getJSONObject(i).getString("cont_sign"));
            contsigns.add(result.getJSONObject(i).getString("cont_sign"));
        }

        Dal dal = new Dal();
        List <Mei> meis=dal.getMeisByContSigns(contsigns,user);
        List <Mei> meis2=new ArrayList<>();
        /*if(true){
            for (Mei mei:meis) {
                if(mei.getTitle().contains("电脑")){
                    meis2.add(mei) ;
                }
            }
            for (Mei mei:meis)
                if(!mei.getTitle().contains("电脑")){
                    meis2.add(mei) ;
                }
        }*/
        for (Mei mei:meis) {
            meis2.add(mei) ;
        }

        request.setAttribute("meiAll",meis2);
        request.setAttribute("user",user);
        request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
        //request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
        return;
    }

    protected void meiAddDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = "";
        String filePath="";

        // 上传文件存储目录
         String UPLOAD_DIRECTORY = "resources";
        // 上传配置
         int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
         int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
         int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

        request.setCharacterEncoding("utf8");
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");

        // 检测是否为多媒体上传

        if (!ServletFileUpload.isMultipartContent(request)) {
            // 如果不是则停止
            PrintWriter writer = response.getWriter();
            writer.println("Error: 表单必须包含 enctype=multipart/form-data");
            writer.flush();
            return;
        }

        // 配置上传参数
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // 设置内存临界值 - 超过后将产生临时文件并存储于临时目录中
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // 设置临时存储目录
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        // 设置最大文件上传值
        upload.setFileSizeMax(MAX_FILE_SIZE);

        // 设置最大请求值 (包含文件和表单数据)
        upload.setSizeMax(MAX_REQUEST_SIZE);

        // 中文处理
        upload.setHeaderEncoding("UTF-8");

        // 构造临时路径来存储上传的文件
        // 这个路径相对当前应用的目录

        String uploadPath = "D:\\IdeaIU\\人工智能\\ai_tao_bao_images\\";
        String imagePath = "/ai_tao_bao_images/";
        String fileName=null;

        // 如果目录不存在则创建
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            // 解析请求的内容提取文件数据
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);

            if (formItems != null && formItems.size() > 0) {
                // 迭代表单数据
                for (FileItem item : formItems) {
                    // 处理不在表单中的字段
                    if (!item.isFormField()) {

                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
                        //System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
                        fileName = df.format(new Date())+new File(item.getName()).getName();
                        filePath = uploadPath + File.separator + fileName;
                        imagePath+=File.separator + fileName;
                        File storeFile = new File(filePath);
                        // 在控制台输出文件的上传路径
                        System.out.println(filePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        request.setAttribute("message",
                                "文件上传成功!");
                    }
                    else {
                        if ("title".equals(item.getFieldName())) {
                            title=item.getString("UTF-8");
                        }
                    }
                }
            }
        } catch (Exception ex) {
            request.setAttribute("message",
                    "错误信息: " + ex.getMessage());
        }




        byte[] fileByte;
        String image = filePath;
        File file = new File(image);
        fileByte = new byte[0];
        try {
            fileByte = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 调用接口
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("brief","{\"name\":\"周杰伦\", \"id\":\"666\"} ");
        map.put("class_id1","1");
        map.put("class_id2","2");
        //String path = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201712%2F15%2F20171215221023_KiYWM.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1619857695&t=0519f8db61b29f1ed5f0aef53cc47505";
        String path = "D:/桌面/desktop/人工智能图片/6d19855411fc6d0f.jpg";

        JSONObject res = client.productAdd(fileByte, map);
        //JSONObject res = client.productAddUrl(path, map);
        //JSONObject res = client.productAddUrl(path, map);
        JSONArray var = new JSONArray();
        System.out.println(res.toString(2));
        String contsign=res.getString("cont_sign");








        String img = "resources/images/" + fileName;

        Dal dal = new Dal();
        dal.MeiAdd(title,user.getPhone(),imagePath,contsign);
        response.sendRedirect("All?method=meiListGo");
    }

    protected void meiAddGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        request.getRequestDispatcher("WEB-INF/mei_add.jsp").forward(request, response);
    }
    protected void meiImageSearchGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        request.getRequestDispatcher("WEB-INF/mei_imagesearch.jsp").forward(request, response);
        return;
    }



    protected void meiPraise(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String meiId=request.getParameter("id");
        Dal dal=new Dal();
        boolean flag=dal.getMeiPraiseFlagByIdAndPhone(meiId,user.getPhone());
        if(flag){//存在
            int number=dal.meiPraisesDel(meiId,user.getPhone());
            response.getWriter().print("{\"flagColor\":0}");
        }
        else{//不存在
            int number=dal.meiPraisesAdd(meiId,user.getPhone());
            response.getWriter().print("{\"flagColor\":1}");
        }
        return;
    }
    protected void meiCollect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String meiId=request.getParameter("id");
        Dal dal=new Dal();
        boolean flag=dal.getMeiCollectFlagByIdAndPhone(meiId,user.getPhone());
        if(flag){//存在
            int number=dal.meiCollectDel(meiId,user.getPhone());
            response.getWriter().print("{\"flagColor\":0}");
        }
        else{//不存在
            int number=dal.meiCollectAdd(meiId,user.getPhone());
            response.getWriter().print("{\"flagColor\":1}");
        }
        return;
    }


    

    protected void meiListGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User) request.getSession().getAttribute(Constants.CURRENT_USER);
        Dal dal=new Dal();
        List <Mei> meis=dal.getMeis(user);
        request.setAttribute("meiAll",meis);
        request.setAttribute("user",user);
        request.getRequestDispatcher("WEB-INF/mei_list.jsp").forward(request, response);

    }


    protected void meiDiyGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/meidiy.jsp").forward(request, response);
    }
    //endregion




    //region About User

    protected void userTixianDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html; charset=utf-8");
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        if(user.getBalance()<50){
            request.getRequestDispatcher("WEB-INF/user_tixian.jsp").forward(request, response);
            return;
        }


        Dal dal=new Dal();
        String money=request.getParameter("money");
        if(money.equals("50")) dal.userTixian(user,50);
        else dal.userTixian(user,50);
        user=dal.getUserByPhone(user.getPhone());
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.getWriter().append("<p>已提现......</p>");
        user=dal.getUserByPhone(user.getPhone());
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.setHeader("Refresh","2;URL=All?method=userInfoGo");
    }

    protected void userTixianGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        request.setAttribute("user", user);
        request.getRequestDispatcher("WEB-INF/user_tixian.jsp").forward(request, response);
    }

    protected void userCollectGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        Dal dal=new Dal();
        List <Mei> meis=dal.getMeiCollectByPhone(user);
        List<Tie> ties=dal.getTieCollectByPhone(user);
        System.out.println(meis);
        request.setAttribute("meiAll",meis);
        request.setAttribute("tieAll",ties);
        System.out.println("meiAll:"+meis);
        request.getRequestDispatcher("WEB-INF/user_collect.jsp").forward(request, response);
    }


    protected void userInfoUpdateDescriptionDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String description=request.getParameter("discription");
        Dal dal=new Dal();
        dal.userChangeDescription(user.getPhone(),description);
        user=dal.getUserByPhone(user.getPhone());
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.sendRedirect("All?method=userInfoGo");
    }

    protected void userInfoUpdateDescriptionGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String value=user.getDescription();
        request.setAttribute("valuedis", value);
        request.getRequestDispatcher("WEB-INF/user_update_discirption.jsp").forward(request, response);
    }

    protected void userInfoUpdateSexDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String sex=request.getParameter("sex");
        Dal dal=new Dal();
        dal.userChangeSex(user.getPhone(),sex);
        user=dal.getUserByPhone(user.getPhone());
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.sendRedirect("All?method=userInfoGo");
    }

    protected void userInfoUpdateSexGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        request.getRequestDispatcher("WEB-INF/user_update_sex.jsp").forward(request, response);
    }

    protected void userInfoUpdateNameDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String name=request.getParameter("name");
        Dal dal=new Dal();
        dal.userChangeName(user.getPhone(),name);
        user=dal.getUserByPhone(user.getPhone());
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.sendRedirect("All?method=userInfoGo");
    }

    protected void userInfoUpdateNameGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String key;
        String value=user.getName();
        request.setAttribute("valuename", value);
        request.getRequestDispatcher("WEB-INF/user_update_name.jsp").forward(request, response);
    }

    protected void userInfoGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);

        if(user==null) {
            response.sendRedirect("All?method=userLoginGo");
        }
        else {
            request.setAttribute("user", user);
            request.getRequestDispatcher("WEB-INF/user_info.jsp").forward(request, response);
        }
    }

    protected void userLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().removeAttribute(Constants.CURRENT_USER);
        response.getWriter().print("{\"flag\":1}");
        return;
    }

    protected void userLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userphone=request.getParameter("userPhone");
        String userpassword=request.getParameter("userPassword");
        Dal dal=new Dal();
        User user=dal.getUserByPhone(userphone);
        if(user==null){
            response.getWriter().append("{\"praiseflag\":0}");
            return;
        }
        else if(!user.getPassword().equals(userpassword)){
            response.getWriter().append("{\"praiseflag\":1}");
            return;
        }
        else{
            request.getSession().setAttribute(Constants.CURRENT_USER,user);
            response.getWriter().append("{\"praiseflag\":2}");
            //response.sendRedirect("/All?method=indexGo");
            return;
        }
    }

    protected void indexGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User) request.getSession().getAttribute(Constants.CURRENT_USER);
        request.setAttribute("user", user);
        Dal dal=new Dal();
        List <Mei> meis=dal.getMeis(user);
        //List <Tie> ties=dal.getTies(user);
        //request.setAttribute("tieAll",ties);
        request.setAttribute("meiAll",meis);
        request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
        return;
    }

    protected void userIndexGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        request.setAttribute("user", user);
        if(user==null){
            System.out.println("indexgo1");
            request.setAttribute("back","back");
            request.getRequestDispatcher("WEB-INF/user_login.jsp").forward(request, response);

        }
        else{
            System.out.println("indexgo2");
            request.getRequestDispatcher("WEB-INF/user_index.jsp").forward(request, response);
            //response.sendRedirect("WEB-INF/user_login.jsp");
        }



    }

    protected void userLoginGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            request.getRequestDispatcher("WEB-INF/user_login.jsp").forward(request, response);
    }


    protected void userRegisterGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.sendRedirect("user_register.jsp");//不知道为啥不能用
        request.getRequestDispatcher("WEB-INF/user_register.jsp").forward(request,response);
    }
    protected void userForgetPasswordGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/user_forgetPassword.jsp").forward(request,response);
    }
    protected void userProcotolGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/user_xieyi.jsp").forward(request,response);
    }
    protected void userRegisterDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String phone=request.getParameter("phone");
        String password=request.getParameter("password");
        String name=request.getParameter("name");
        String sex=request.getParameter("sex");
        Dal dal=new Dal();
        User user=dal.getUserByPhone(phone);
        if(user!=null){
            response.getWriter().append("{\"flag\":0}");
            return;
        }
        int result=dal.userAdd(phone,password,name,sex);
        if(result==1){
            response.getWriter().append("{\"flag\":1}");
            return;
        }
    }

    protected void userForgetPasswordDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String phone=request.getParameter("phone");
        String password=request.getParameter("password");
        if(password==null||password.isEmpty()){
            Dal dal=new Dal();
            User user=dal.getUserByPhone(phone);
            if(user==null){
                response.getWriter().append("{\"flag\":0}");
                return;
            }
            else{
                response.getWriter().append("{\"flag\":1}");
                return;
            }
        }

        Dal dal=new Dal();
        int number=dal.changePassword(phone,password);
        if(number==1){
            response.getWriter().append("{\"flag\":1}");
            return;
        }
        else{
            response.getWriter().append("{\"flag\":0}");
            return;
        }
    }
    //endregion
//##################################

    //region About Tie

    protected void tieAddDo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 上传文件存储目录
        String title="";
        String UPLOAD_DIRECTORY = "resources";
        // 上传配置
        int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
        int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
        int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

        request.setCharacterEncoding("utf8");
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");

        // 检测是否为多媒体上传
        if (!ServletFileUpload.isMultipartContent(request)) {
            // 如果不是则停止
            PrintWriter writer = response.getWriter();
            writer.println("Error: 表单必须包含 enctype=multipart/form-data");
            writer.flush();
            return;
        }

        // 配置上传参数
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // 设置内存临界值 - 超过后将产生临时文件并存储于临时目录中
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // 设置临时存储目录
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        // 设置最大文件上传值
        upload.setFileSizeMax(MAX_FILE_SIZE);

        // 设置最大请求值 (包含文件和表单数据)
        upload.setSizeMax(MAX_REQUEST_SIZE);

        // 中文处理
        upload.setHeaderEncoding("UTF-8");

        // 构造临时路径来存储上传的文件
        // 这个路径相对当前应用的目录

        String uploadPath = getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY+File.separator +"images";
        String fileName=null;

        // 如果目录不存在则创建
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            // 解析请求的内容提取文件数据
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);

            if (formItems != null && formItems.size() > 0) {
                // 迭代表单数据
                for (FileItem item : formItems) {
                    // 处理不在表单中的字段
                    if (!item.isFormField()) {
                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
                        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
                        fileName = df.format(new Date())+new File(item.getName()).getName();
                        String filePath = uploadPath + File.separator + fileName;
                        File storeFile = new File(filePath);
                        // 在控制台输出文件的上传路径
                        System.out.println(filePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        request.setAttribute("message",
                                "文件上传成功!");
                    }
                    else {
                        if ("title".equals(item.getFieldName())) {
                            title=item.getString("UTF-8");
                        }
                    }
                }
            }

        } catch (Exception ex) {
            request.setAttribute("message",
                    "错误信息: " + ex.getMessage());
        }





        String img = "resources/images/" + fileName;
        Dal dal = new Dal();
        dal.TieAdd(title,user.getPhone(),img);
        response.sendRedirect("All?method=tieDiyGo");
    }

    protected void tieAddGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        request.getRequestDispatcher("WEB-INF/tie_add.jsp").forward(request, response);
    }

    protected void tieMineDel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User) request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String tieId=request.getParameter("id");
        Dal dal=new Dal();
        dal.tieMineDel(tieId);

    }

    protected void tieMineGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=null;
        user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        else {
            Dal dal=new Dal();
            List <Tie> ties=null;
            ties=dal.getTieMineList(user);
            request.setAttribute("tieAll",ties);
            System.out.println("TieMyAll:"+ties);
            request.getRequestDispatcher("WEB-INF/tie_my.jsp").forward(request, response);
        }
    }
    
    protected void tiePraise(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String tieId=request.getParameter("id");
        Dal dal=new Dal();
        boolean flag=dal.getTiePraiseFlagByIdAndPhone(tieId,user.getPhone());
        if(flag){//存在
            int number=dal.tiePraisesDel(tieId,user.getPhone());
            response.getWriter().print("{\"flagColor\":0}");
        }
        else{//不存在
            int number=dal.tiePraisesAdd(tieId,user.getPhone());
            response.getWriter().print("{\"flagColor\":1}");
        }
        return;
    }
    protected void tieCollect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User)request.getSession().getAttribute(Constants.CURRENT_USER);
        if(user==null) response.sendRedirect("All?method=userLoginGo");
        String tieId=request.getParameter("id");
        Dal dal=new Dal();
        boolean flag=dal.getTieCollectFlagByIdAndPhone(tieId,user.getPhone());
        if(flag){//存在
            int number=dal.tieCollectDel(tieId,user.getPhone());
            response.getWriter().print("{\"flagColor\":0}");
        }
        else{//不存在
            int number=dal.tieCollectAdd(tieId,user.getPhone());
            response.getWriter().print("{\"flagColor\":1}");
        }
        return;
    }

    protected void tieDiyGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user=(User) request.getSession().getAttribute(Constants.CURRENT_USER);
        request.setAttribute("user", user);
        Dal dal=new Dal();
        request.setAttribute("user",user);
        List <Tie> ties=dal.getTies(user);
        request.setAttribute("tieAll",ties);
        request.getRequestDispatcher("WEB-INF/tiediy.jsp").forward(request, response);
    }

    protected void tieDetailGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute(Constants.CURRENT_USER);
        request.setAttribute("user", user);
        String tieId=request.getParameter("tieId");
        Dal dal=new Dal();
        Tie tie=dal.getTieById(tieId,user);
        String phone= tie.getPhone();
        User framer=dal.getUserByPhone(phone);
        request.setAttribute("framer", framer);
        request.setAttribute("tie", tie);
        request.getRequestDispatcher("WEB-INF/tiedetail.jsp").forward(request, response);
    }
    //endregion

}
