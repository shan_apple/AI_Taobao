package cn.edu.lit.model;

public class Tie {
    private int id;
    private String phone;
    private String title;
    private int praise;
    private int collect;
    private String img;
    private String username;
    private String photo;
    private boolean praiseFlag;
    private boolean collectFlag;
    private String praiseColor;
    private String collectColor;

    public String getPraiseColor() {
        if(praiseFlag) return "#FF0000";
        else return "#646464";
    }

    public void setPraiseColor(String praiseColor) {
        this.praiseColor = praiseColor;
    }

    public String getCollectColor() {
        if(collectFlag) return "#FF0000";
        else return "#646464";
    }

    public void setCollectColor(String collectColor) {
        this.collectColor = collectColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPraise() {
        return praise;
    }

    public void setPraise(int praise) {
        this.praise = praise;
    }

    public int getCollect() {
        return collect;
    }

    public void setCollect(int collect) {
        this.collect = collect;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isPraiseFlag() {
        return praiseFlag;
    }

    public void setPraiseFlag(boolean praiseFlag) {
        this.praiseFlag = praiseFlag;
    }

    public boolean isCollectFlag() {
        return collectFlag;
    }

    public void setCollectFlag(boolean collectFlag) {
        this.collectFlag = collectFlag;
    }

    public Tie() {
    }

    public Tie(int id, String phone, String title, int praise, int collect, String img, String username, String photo) {
        this.id = id;
        this.phone = phone;
        this.title = title;
        this.praise = praise;
        this.collect = collect;
        this.img = img;
        this.username = username;
        this.photo = photo;
    }


    @Override
    public String toString() {
        return "Tie{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", title='" + title + '\'' +
                ", praise=" + praise +
                ", collect=" + collect +
                ", img='" + img + '\'' +
                ", username='" + username + '\'' +
                ", photo='" + photo + '\'' +
                ", praiseFlag=" + praiseFlag +
                ", collectFlag=" + collectFlag +
                '}';
    }
}
