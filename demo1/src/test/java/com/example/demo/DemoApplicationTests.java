package com.example.demo;

import com.baidu.aip.imagesearch.AipImageSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class DemoApplicationTests {
    //设置APPID/AK/SK
    public static final String APP_ID = "23900832";
    public static final String API_KEY = "9Kqjuyn2F8HVzzqygdNynj41";
    public static final String SECRET_KEY = "yMVVMKKpWlOP4mdj5bbILXqY3GLKyLNH";
    // 初始化一个AipImageSearch
    AipImageSearch client = new AipImageSearch(APP_ID, API_KEY, SECRET_KEY);

    @Before
    void beforeall(){
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
        //client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
        //client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理
        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");
    }


    @Test
    void tojsp(){

    }

    @Test
        //商品图片删除
    void test3 () throws JSONException {
        // 调用接口
        // 删除商品，传入参数为图片签名
        // 传入可选参数调用接口
        String contSign="2628325146,241710465";
        HashMap<String, String> options = new HashMap<String, String>();
        JSONObject res = client.productDeleteBySign(contSign, options);
        System.out.println(res.toString(2));
    }

    @Test
        //商品图片上传
    void test2 () throws JSONException {
        byte[] fileByte;
        String image = "D:\\image\\a.jpg";
        File file = new File(image);
        fileByte = new byte[0];
        try {
            fileByte = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 调用接口
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("brief","{\"name\":\"周杰伦\", \"id\":\"666\"} ");
        map.put("class_id1","1");
        map.put("class_id2","2");
        //String path = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201712%2F15%2F20171215221023_KiYWM.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1619857695&t=0519f8db61b29f1ed5f0aef53cc47505";
        String path = "D:/桌面/desktop/人工智能图片/6d19855411fc6d0f.jpg";

        JSONObject res = client.productAdd(fileByte, map);
        //JSONObject res = client.productAddUrl(path, map);
        //JSONObject res = client.productAddUrl(path, map);
        JSONArray var = new JSONArray();
        System.out.println(res.toString(2));
        System.out.println(res.getString("cont_sign"));
        //System.out.println(res.toString(2));
        /**
         * {
         *   "log_id": 3145897339619123621,
         *   "cont_sign": "2628325146,241710465"
         * }
         */
    }

    @Test
    //商品图片搜索
    void test1 () throws JSONException {
        // 调用接口
        System.out.println(this.getClass().getClassLoader().getResource(""));
        String path = "D:\\IdeaIU\\人工智能\\demo1\\src\\test\\java\\com\\example\\demo\\image\\杯子.jpg";
        JSONObject res = client.productSearch(path, new HashMap<String, String>());
        //System.out.println(res.getJSONArray("result"));
        JSONArray result=res.getJSONArray("result");
        List<String> contsigns=new ArrayList<String>();
        for (int i=0;i<result.length();i++){
            System.out.println(result.getJSONObject(i).getString("cont_sign"));
            contsigns.add(result.getJSONObject(i).getString("cont_sign"));
        }
        /**
         * {
         *   "result": [
         *     {
         *       "brief": "{\"name\":\"周杰伦\", \"id\":\"666\"} ",
         *       "score": 0.64548992812634,
         *       "cont_sign": "2628325146,241710465"
         *     },
         *     {
         *       "brief": "这是卫生纸",
         *       "score": 0.61330957859755,
         *       "cont_sign": "2981486938,3763138945"
         *     },
         *     {
         *       "brief": "这是鼠标",
         *       "score": 0.59719273690134,
         *       "cont_sign": "14761571,4199205197"
         *     }
         *   ],
         *   "log_id": 2987644640529530405,
         *   "has_more": false,
         *   "result_num": 3
         * }
         */
    }
    
    
    @Test
    //相同图片搜索
    void samplepicture() throws JSONException {
        // 调用接口
        String path = "C:\\Users\\南枝\\Pictures\\Camera Roll\\b.jpg";
        JSONObject res = client.sameHqSearch(path, new HashMap<String, String>());
        System.out.println(res.toString(2));
        System.out.println("------------------------------");
        System.out.println(res.toString(1));
        System.out.println("------------------------------");
        System.out.println(res.toString());
        System.out.println("------------------------------");
        System.out.println("------------------------------");

    }

}
