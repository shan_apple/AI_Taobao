<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
	<title>美拍</title>
	<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
	<link href="resources/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <header id="header">
  	<a href="javascript:" onclick="self.location=document.referrer;" class="iconfont fl">&#xe63f;</a>
    <div class="title">注册</div>
    <a href="#" class="iconfont fr">&#xe6a0;</a>
  </header> 
  <div class="content">
  	 <div class="register-box">
  	 	 <div class="form-box">
  	 	 <form name="form1" action="useradd.do" method="post">
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe64f;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input type="text" class="input_box"  placeholder="输入手机号/邮箱/昵称" id="phone">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input type="password" class="input_box" placeholder="输入密码" id="password">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe692;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input type="password" class="input_box" placeholder="再次输入密码" id="password1">
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe608;</i></div>
  	 	 		<div class="row-r">
  	 	 			<input class="input_box"  placeholder="输入昵称" id="name">
  	 	 		</div>
  	 	 	</div><div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe6a8;</i></div>
  	 	 		<div class="row-r" id="sexs">
  	 	 			<span>性别</span>
  	 	 			<label><input class="btn-radio" type="radio" class="sexs" name="sex" checked value="男">男</label>
                    <label><input class="btn-radio" type="radio" class="sexs" name="sex" value="女">女</label>
  	 	 		</div>
  	 	 	</div><div class="row row-input clearfix">
  	 	 		<div class="row-l fl"><i class="iconfont">&#xe60f;</i></div>
  	 	 		<div class="row-r">
  	 	 			<span>支付方式</span>
  	 	 			<label style="color: #24a82e;"><input class="btn-radio" type="radio" name="radio2" checked>微信</label>
                    <label style="color: #2d95de;"><input class="btn-radio" type="radio" name="radio2">银行卡</label>
  	 	 		</div>
  	 	 	</div>
  	 	 	<div class="row row-txt" id="agree">
  	 	 		<p><input class="checkbox" type="checkbox" name="checkbox">已阅读并同意<a href="All?method=userProcotolGo" target ="_blank">《微风向用户协议》</a>
  	 	 		
  	 	 		</p>
  	 	 	</div>
  	 	 	<div class="row row-btn">
  	 	 		<a href="javascript:void(0);"class="green-btn" id="tip">确定</a>
  	 	 	</div>
  	 	 	</form>
  	 	 </div>
  	 </div>
     <div class="register-table">
     	<table>
     		<tr><th>非会员/会员权限对比</th><th>非会员</th><th>会员</th></tr>
     		<tr>
     			<td class="td-1">无限使用<span>贴贴</span>功能</td>
     			<td class="td-2">仅限浏览</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">无限使用<span>美拍</span>功能</td>
     			<td class="td-2">仅限浏览</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">无限使用<span>美言</span>功能</td>
     			<td class="td-2">仅限浏览</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">无限使用<span>美视</span>功能</td>
     			<td class="td-2">仅限浏览购买</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">无限使用<span>美视</span>功能</td>
     			<td class="td-2">仅限浏览购买</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">一键生成<span>推广</span>二维码</td>
     			<td class="td-2">×</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">分销系统帮你<span>赚钱</span></td>
     			<td class="td-2">×</td>
     			<td class="td-3">无限使用</td>
     		</tr>
     		<tr>
     			<td class="td-1">社区<span>促销奖励</span>活动</td>
     			<td class="td-2">×</td>
     			<td class="td-3">√</td>
     		</tr>
     		<tr>
     			<td class="td-1"><span>会费</span></td>
     			<td class="td-2">免费</td>
     			<td class="td-3">50元一次性</td>
     		</tr>
     	</table>
     </div>
  </div>
 
  <%@ include file="foot.jsp" %>


  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <script src="resources/layui/layui.js" charset="utf-8"></script>
  <script >
	  document.getElementById("foot4").classList.add("active");
	  layui.use('layer', function(){ //独立版的layer无需执行这一句
		  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
		  //触发事件
		  function success () {
			  layer.msg("注册成功", {
				  icon:1,
				  offset: 't',//设置距离 上,左 位置['40%','30%']
				  anim: 5//出现方式为渐显
			  });
		  }
		  function error (str) {
			  layer.msg(str, {
				  offset: 't',//设置距离 上,左 位置['40%','30%']
				  anim: 6//出现方式为抖动
			  });
		  }


		  $("#tip").on('click', function (message){
			  var phone=document.getElementById("phone").value;
			  var password=document.getElementById("password").value;
			  var password1=document.getElementById("password1").value;
			  var name=document.getElementById("name").value;
			  var sexs=document.getElementById("sexs").getElementsByTagName("input");
			  var sex;
			  for(var i=0;i<sexs.length;i++){
			  	if(sexs[i].checked){
			  		sex=sexs[i].value;
			  		break;
				}
			  }
			  var agree=document.getElementById("agree").getElementsByTagName("input");


			  //isNaN(userPhone)判断是否全为数字
			  if(phone==null||phone.length!=11||isNaN(phone)){
				  error("手机号填写错误");
				  return ;
			  }
			  if(password==null||password.length<6){
				  error("密码长度不得小于六位");
				  return;
			  };
			  if(password!=password1){
			  	error("两次密码不匹配");
			  	return;
			  }
			  if(name==null||name.length==1){
			  	error("昵称不可为空")
			  }
			  if(!agree[0].checked){
			  	error("请先阅读并同意用户协议");
			  	return;
			  }

			  $.post("All?method=userRegisterDo",
					  {
						  phone:phone,
						  password:password,
						  name:name,
						  sex:sex
					  },
					  function(data,status){
						  var number=data.flag;
						  if(number=="0"){
							  error("已存在该用户");
						  }
						  else if(number=="1"){
							  success();
						  }
					  }
					  ,"json"
			  );

		  });
	  });
  </script>


</body>
</html>
